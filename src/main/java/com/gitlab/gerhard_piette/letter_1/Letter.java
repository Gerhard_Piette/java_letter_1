package com.gitlab.gerhard_piette.letter_1;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;

public interface Letter {

	/**
	 * @param data
	 * @param offset
	 * @return
	 * @throws DefectLetter In case of a bad letter number or bad letter encoding. The function might not check if the letter is invalid.
	 * @throws DefectOffset
	 */
	public LetterEnd read(byte[] data, int offset) throws DefectLetter, DefectOffset;

	/**
	 * The first bytes of the data array are written.
	 * An array and not an integer is used because encoding is typically used to create an array of bytes.
	 *
	 * @param letter
	 * @param data
	 * @return
	 * @throws DefectLetter In case of a bad letter number or bad letter encoding. The function might not check if the letter is invalid.
	 * @throws DefectOffset In case the array is too small.
	 */
	public int write(byte[] data, int offset, int letter) throws DefectLetter, DefectOffset;
}
