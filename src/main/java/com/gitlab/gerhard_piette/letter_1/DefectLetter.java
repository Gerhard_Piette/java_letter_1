package com.gitlab.gerhard_piette.letter_1;

/**
 * A defect related to a letter number or letter encoding.
 */
public class DefectLetter extends Exception {

	public int letterOffset;

	public DefectLetter(int letterOffset) {
		this.letterOffset = letterOffset;
	}

}