package com.gitlab.gerhard_piette.letter_1;


/**
 * The class is called LetterEnd because:
 *
 * 1)
 * It is used for reading text letter by letter.
 *
 * 2)
 * The letter is represented by a number and not this class.
 *
 * 3)
 * The end offset is given because it is the most useful information for reading text letter by letter.
 * The letter length information is less good because:
 * - It would require "offset = offset + length" operations for reading a text.
 * - The length information depends on the encoding and not only on the letter.
 *
 * 4)
 * This class should not be used to store information about defects.
 * For defects or errors: Use the standard defect or error handling mechanism of the language.
 *
 */
public class LetterEnd {

	/**
	 * The letter number.
	 */
	public int letter = 0;

	/**
	 * The unit offset after the letter or the length of the text in case the end of text has been reached.
	 */
	public int end = 0;

	public LetterEnd() {

	}

	public LetterEnd(int letter, int end) {
		this.letter = letter;
		this.end = end;
	}
}
